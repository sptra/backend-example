package com.nostratech.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class BackendExampleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendExampleAppApplication.class, args);
	}
}
