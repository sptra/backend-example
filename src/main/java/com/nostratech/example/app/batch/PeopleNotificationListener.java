package com.nostratech.example.app.batch;

import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by yukibuwana on 2/1/17.
 */

@Component
public class PeopleNotificationListener extends JobExecutionListenerSupport {

    private static final Logger log = LoggerFactory.getLogger(PeopleNotificationListener.class);
    private final PeopleRepository peopleRepository;

    @Autowired
    public PeopleNotificationListener(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("JOB FINISHED! Time to verify the results");

            List<People> peopleList = peopleRepository.findAll();

            for (People people : peopleList) {
                log.info("Found <" + people + "> in the database.");
            }
        }
    }
}
