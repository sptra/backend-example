package com.nostratech.example.app.persistence.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ADMINDUK")
@DynamicUpdate
@Data
public class Adminduk extends Base {
    @Column(name = "NIK_AYAH")
    private String nik_ayah;

    @Column(name = "JENIS_KLMIN")
    private String jenis_klmin;

    @Column(name = "NO_RW")
    private String no_rw;

    @Column(name = "NO_RT")
    private String no_rt;

    @Column(name = "TELP")
    private String telp;

    @Column(name = "STAT_HBKEL")
    private String stat_hbkel;

    @Column(name = "AGAMA")
    private String agama;

    @Column(name = "TMPT_LHR")
    private String tmpt_lhr;

    @Column(name = "JENIS_PKRJN")
    private String jenis_pkrjn;

    @Column(name = "NIK")
    private String nik;

    @Column(name = "NAMA_LGKP_IBU")
    private String nama_lgkp_ibu;

    @Column(name = "STAT_KWN")
    private String stat_kwn;

    @Column(name = "NO_KK")
    private String no_kk;

    @Column(name = "PDDK_AKH")
    private String pddk_akh;

    @Column(name = "NAMA_LGKP_AYAH")
    private String nama_lgkp_ayah;

    @Column(name = "NAMA_KEP")
    private String nama_kep;

    @Column(name = "DUSUN")
    private String dusun;

    @Column(name = "TGL_LHR")
    private String tgl_lhr;

    @Column(name = "NIK_IBU")
    private String nik_ibu;

    @Column(name = "NO_PROP")
    private String no_prop;

    @Column(name = "KODE_POS")
    private String kode_pos;

    @Column(name = "NO_KEL")
    private String no_kel;

    @Column(name = "NAMA_LGKP")
    private String nama_lgkp;

    @Column(name = "ALAMAT")
    private String alamat;

    @Column(name = "NO_KAB")
    private String no_kab;

    @Column(name = "GOL_DRH")
    private String gol_drh;

    @Column(name = "NO_KEC")
    private String no_kec;

    @Column(name = "_INDEX")
    private String _index;

    @Column(name = "_TYPE")
    private String _type;

    @Column(name = "_ID")
    private String _id;

    @Column(name = "_SCORE")
    private String _score;

    public Adminduk(String nik_ayah, String jenis_klmin, String no_rw, String no_rt, String telp, String stat_hbkel, String agama, String tmpt_lhr, String jenis_pkrjn, String nik, String nama_lgkp_ibu, String stat_kwn, String no_kk, String pddk_akh, String nama_lgkp_ayah, String nama_kep, String dusun, String tgl_lhr, String nik_ibu, String no_prop, String kode_pos, String no_kel, String nama_lgkp, String alamat, String no_kab, String gol_drh, String no_kec, String _index, String _type, String _id, String _score) {
        this.nik_ayah = nik_ayah;
        this.jenis_klmin = jenis_klmin;
        this.no_rw = no_rw;
        this.no_rt = no_rt;
        this.telp = telp;
        this.stat_hbkel = stat_hbkel;
        this.agama = agama;
        this.tmpt_lhr = tmpt_lhr;
        this.jenis_pkrjn = jenis_pkrjn;
        this.nik = nik;
        this.nama_lgkp_ibu = nama_lgkp_ibu;
        this.stat_kwn = stat_kwn;
        this.no_kk = no_kk;
        this.pddk_akh = pddk_akh;
        this.nama_lgkp_ayah = nama_lgkp_ayah;
        this.nama_kep = nama_kep;
        this.dusun = dusun;
        this.tgl_lhr = tgl_lhr;
        this.nik_ibu = nik_ibu;
        this.no_prop = no_prop;
        this.kode_pos = kode_pos;
        this.no_kel = no_kel;
        this.nama_lgkp = nama_lgkp;
        this.alamat = alamat;
        this.no_kab = no_kab;
        this.gol_drh = gol_drh;
        this.no_kec = no_kec;
        this._index = _index;
        this._type = _type;
        this._id = _id;
        this._score = _score;
    }

    public Adminduk() {
    }

    public String getNik_ayah() {
        return nik_ayah;
    }

    public void setNik_ayah(String nik_ayah) {
        this.nik_ayah = nik_ayah;
    }

    public String getJenis_klmin() {
        return jenis_klmin;
    }

    public void setJenis_klmin(String jenis_klmin) {
        this.jenis_klmin = jenis_klmin;
    }

    public String getNo_rw() {
        return no_rw;
    }

    public void setNo_rw(String no_rw) {
        this.no_rw = no_rw;
    }

    public String getNo_rt() {
        return no_rt;
    }

    public void setNo_rt(String no_rt) {
        this.no_rt = no_rt;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getStat_hbkel() {
        return stat_hbkel;
    }

    public void setStat_hbkel(String stat_hbkel) {
        this.stat_hbkel = stat_hbkel;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getTmpt_lhr() {
        return tmpt_lhr;
    }

    public void setTmpt_lhr(String tmpt_lhr) {
        this.tmpt_lhr = tmpt_lhr;
    }

    public String getJenis_pkrjn() {
        return jenis_pkrjn;
    }

    public void setJenis_pkrjn(String jenis_pkrjn) {
        this.jenis_pkrjn = jenis_pkrjn;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama_lgkp_ibu() {
        return nama_lgkp_ibu;
    }

    public void setNama_lgkp_ibu(String nama_lgkp_ibu) {
        this.nama_lgkp_ibu = nama_lgkp_ibu;
    }

    public String getStat_kwn() {
        return stat_kwn;
    }

    public void setStat_kwn(String stat_kwn) {
        this.stat_kwn = stat_kwn;
    }

    public String getNo_kk() {
        return no_kk;
    }

    public void setNo_kk(String no_kk) {
        this.no_kk = no_kk;
    }

    public String getPddk_akh() {
        return pddk_akh;
    }

    public void setPddk_akh(String pddk_akh) {
        this.pddk_akh = pddk_akh;
    }

    public String getNama_lgkp_ayah() {
        return nama_lgkp_ayah;
    }

    public void setNama_lgkp_ayah(String nama_lgkp_ayah) {
        this.nama_lgkp_ayah = nama_lgkp_ayah;
    }

    public String getNama_kep() {
        return nama_kep;
    }

    public void setNama_kep(String nama_kep) {
        this.nama_kep = nama_kep;
    }

    public String getDusun() {
        return dusun;
    }

    public void setDusun(String dusun) {
        this.dusun = dusun;
    }

    public String getTgl_lhr() {
        return tgl_lhr;
    }

    public void setTgl_lhr(String tgl_lhr) {
        this.tgl_lhr = tgl_lhr;
    }

    public String getNik_ibu() {
        return nik_ibu;
    }

    public void setNik_ibu(String nik_ibu) {
        this.nik_ibu = nik_ibu;
    }

    public String getNo_prop() {
        return no_prop;
    }

    public void setNo_prop(String no_prop) {
        this.no_prop = no_prop;
    }

    public String getKode_pos() {
        return kode_pos;
    }

    public void setKode_pos(String kode_pos) {
        this.kode_pos = kode_pos;
    }

    public String getNo_kel() {
        return no_kel;
    }

    public void setNo_kel(String no_kel) {
        this.no_kel = no_kel;
    }

    public String getNama_lgkp() {
        return nama_lgkp;
    }

    public void setNama_lgkp(String nama_lgkp) {
        this.nama_lgkp = nama_lgkp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_kab() {
        return no_kab;
    }

    public void setNo_kab(String no_kab) {
        this.no_kab = no_kab;
    }

    public String getGol_drh() {
        return gol_drh;
    }

    public void setGol_drh(String gol_drh) {
        this.gol_drh = gol_drh;
    }

    public String getNo_kec() {
        return no_kec;
    }

    public void setNo_kec(String no_kec) {
        this.no_kec = no_kec;
    }

    public String get_index() {
        return _index;
    }

    public void set_index(String _index) {
        this._index = _index;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_score() {
        return _score;
    }

    public void set_score(String _score) {
        this._score = _score;
    }
}
