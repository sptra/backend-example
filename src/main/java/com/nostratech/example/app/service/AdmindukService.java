package com.nostratech.example.app.service;

import com.nostratech.example.app.converter.AdmindukVoConverter;
import com.nostratech.example.app.converter.IBaseVoConverter;
import com.nostratech.example.app.converter.PeopleVoConverter;
import com.nostratech.example.app.exception.BackendExampleException;
import com.nostratech.example.app.persistence.domain.Adminduk;
import com.nostratech.example.app.persistence.domain.Base;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.AdmindukRepository;
import com.nostratech.example.app.persistence.repository.BaseRepository;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import com.nostratech.example.app.validator.PeopleValidator;
import com.nostratech.example.app.vo.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by yukibuwana on 2/2/17.
 */
@Transactional(readOnly=false)
@Service
public class AdmindukService extends BaseService<Adminduk, AdmindukResponseVO, AdmindukRequestVO> {

    @Autowired
    AdmindukVoConverter admindukVoConverter;

    @Autowired
    PeopleValidator peopleValidator;

    @Autowired
    AdmindukRepository admindukRepository;

    @Override
    protected BaseRepository<Adminduk> getJpaRepository() {
        return admindukRepository;
    }

    @Override
    protected JpaSpecificationExecutor<Adminduk> getSpecRepository() {
        return admindukRepository;
    }

    @Override
    protected IBaseVoConverter<AdmindukRequestVO, AdmindukResponseVO, Adminduk> getVoConverter() {
        return admindukVoConverter;
    }

    @Override
    public AdmindukResponseVO add(AdmindukRequestVO admindukRequestVO) {

        /*
        // validate input
        String messageValidation = peopleValidator.validateAdd(people);
        if (null != messageValidation) throw new BackendExampleException(messageValidation);
        */
        return super.add(admindukRequestVO);
    }



    @Override
    public AdmindukResponseVO update(String id, AdmindukRequestVO vo) {

        /*
        // validate input
        String messageValidation = peopleValidator.validateUpdate(vo);
        if (null != messageValidation) throw new BackendExampleException(messageValidation);
        */
        return super.update(id, vo);
    }

    public boolean isIDAvailable(String id) {
        boolean available=false;
        // get all distinct _id
        List<String> _idList = admindukRepository.find_idDistinct();

        for (String _id : _idList) {
            if (id.equals(_id)){
                available=true;
                break;
            }
        }

        return available;
    }

    public String insertAdminduk(String input){
        String output="";
        JSONObject jsonObject = new JSONObject(input);
        JSONArray jsonArray=jsonObject.getJSONArray("data");
        List<AdmindukRequestVO> admindukRequestVOS = new ArrayList<>();
        int inserted=0;
        int udpated=0;

        for (int i=0;i<jsonArray.length();i++){
            JSONObject object = jsonArray.getJSONObject(i);
            AdmindukRequestVO admindukRequestVO= new AdmindukRequestVO();

            admindukRequestVO.setNik_ayah(object.get("nik_ayah").toString());
            admindukRequestVO.setJenis_klmin(object.get("jenis_klmin").toString());
            admindukRequestVO.setNo_rw(object.get("no_rw").toString());
            admindukRequestVO.setNo_rt(object.get("no_rt").toString());
            admindukRequestVO.setTelp(object.get("telp").toString());

            admindukRequestVO.setStat_hbkel(object.get("stat_hbkel").toString());
            admindukRequestVO.setAgama(object.get("agama").toString());
            admindukRequestVO.setTmpt_lhr(object.get("tmpt_lhr").toString());
            admindukRequestVO.setJenis_pkrjn(object.get("jenis_pkrjn").toString());
            admindukRequestVO.setNik(object.get("nik").toString());

            admindukRequestVO.setNama_lgkp_ibu(object.get("nama_lgkp_ibu").toString());
            admindukRequestVO.setStat_kwn(object.get("stat_kwn").toString());
            admindukRequestVO.setNo_kk(object.get("no_kk").toString());
            admindukRequestVO.setPddk_akh(object.get("pddk_akh").toString());
            admindukRequestVO.setNama_lgkp_ayah(object.get("nama_lgkp_ayah").toString());

            admindukRequestVO.setNama_kep(object.get("nama_kep").toString());
            admindukRequestVO.setDusun(object.get("dusun").toString());
            admindukRequestVO.setTgl_lhr(object.get("tgl_lhr").toString());
            admindukRequestVO.setNik_ibu(object.get("nik_ibu").toString());
            admindukRequestVO.setNo_prop(object.get("no_prop").toString());

            admindukRequestVO.setKode_pos(object.get("kode_pos").toString());
            admindukRequestVO.setNo_kel(object.get("no_kel").toString());
            admindukRequestVO.setNama_lgkp(object.get("nama_lgkp").toString());
            admindukRequestVO.setAlamat(object.get("alamat").toString());
            admindukRequestVO.setNo_kab(object.get("no_kab").toString());

            admindukRequestVO.setGol_drh(object.get("gol_drh").toString());
            admindukRequestVO.setNo_kec(object.get("no_kec").toString());
            admindukRequestVO.set_index(object.get("_index").toString());
            admindukRequestVO.set_type(object.get("_type").toString());
            admindukRequestVO.set_id(object.get("_id").toString());

            admindukRequestVO.set_score(object.get("_score").toString());

            Adminduk adminduk=new Adminduk();
            System.out.println(admindukRequestVO.getId());
            if(isIDAvailable(admindukRequestVO.get_id())==true){
                admindukRequestVO.setVersion(admindukRepository.get_version(admindukRequestVO.get_id()));
                admindukRequestVO.setId(admindukRepository.get_id(admindukRequestVO.get_id()));
                update(admindukRequestVO.getId(),admindukRequestVO);
                udpated++;
            }else{
                add(admindukRequestVO);
                inserted++;
            }

        }
        output=inserted+" Inserted and "+udpated+" Updated";
        return output;
    }
}
