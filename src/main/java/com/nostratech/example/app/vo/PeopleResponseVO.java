package com.nostratech.example.app.vo;

import lombok.Data;

/**
 * Created by yukibuwana on 1/30/17.
 */

@Data
public class PeopleResponseVO extends BaseVO {

    String name;
    String address;
}
