package com.nostratech.example.app.vo;

import lombok.Data;

/**
 * Created by yukibuwana on 1/24/17.
 */

@Data
public class HTMLToDocVO {
    private String fileName;
    private String nip;
    private String docType;
    private String input;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
