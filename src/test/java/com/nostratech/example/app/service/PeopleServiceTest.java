package com.nostratech.example.app.service;

import com.nostratech.example.app.exception.BackendExampleException;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.persistence.repository.PeopleRepository;
import com.nostratech.example.app.vo.PeopleRequestVO;
import com.nostratech.example.app.vo.PeopleResponseVO;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

/**
 * Created by rizkimuhammad on 3/31/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PeopleServiceTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Autowired
    PeopleService peopleService;

    @Autowired
    PeopleRepository peopleRepository;

    private PeopleRequestVO peopleRequestVO;

    @Before
    public void setup() {
        // create People object
        if (peopleRequestVO == null) {
            peopleRequestVO = new PeopleRequestVO();
        }
        peopleRequestVO.setName("name");
        peopleRequestVO.setAddress("address");
    }

    @Test
    public void stage1Add() {
        PeopleRequestVO added = peopleRequestVO;

        PeopleResponseVO result = peopleService.add(added);

        assertTrue(result != null);
    }

    @Test
    public void stage1AddVariant01() {
        PeopleRequestVO added = peopleRequestVO;
        added.setName(null);

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Name is required");
        PeopleResponseVO result = peopleService.add(added);
    }

    @Test
    public void stage1AddVariant02() {
        PeopleRequestVO added = peopleRequestVO;
        added.setAddress(null);

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Address is required");
        PeopleResponseVO result = peopleService.add(added);
    }

    @Test
    public void stage1AddVariant04() {
        StringBuffer value = new StringBuffer();
        for (int i = 0; i <= 20; i++) {
            value.append("A");
        }

        PeopleRequestVO added = peopleRequestVO;
        added.setName(value.toString());

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Name maximum 20 character");
        PeopleResponseVO result = peopleService.add(added);
    }

    @Test
    public void stage1AddVariant05() {
        StringBuffer value = new StringBuffer();
        for (int i = 0; i <= 50; i++) {
            value.append("A");
        }

        PeopleRequestVO added = peopleRequestVO;
        added.setAddress(value.toString());

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Address maximum 50 character");
        PeopleResponseVO result = peopleService.add(added);
    }

    @Test
    public void stage1AddVariant06() {

        PeopleRequestVO added = peopleRequestVO;

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Name : name can't be duplicate");
        PeopleResponseVO result = peopleService.add(added);
    }

    @Test
    public void stage3Update() {
        PeopleRequestVO update = peopleRequestVO;
        People model = peopleRepository.findByName(update.getName());

        PeopleRequestVO vo = new PeopleRequestVO();
        vo.setAddress("Edit for Address");
        vo.setVersion(model.getVersion());

        PeopleResponseVO result = peopleService.update(model, vo);

        assertTrue(result != null);
    }

    @Test
    public void stage3UpdateVariant01() {
        StringBuffer value = new StringBuffer();
        for (int i = 0; i <= 20; i++) {
            value.append("A");
        }

        PeopleRequestVO update = peopleRequestVO;
        People model = peopleRepository.findByName(update.getName());

        PeopleRequestVO vo = new PeopleRequestVO();
        vo.setName(value.toString());
        vo.setVersion(model.getVersion());

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Name maximum 20 character");
        PeopleResponseVO result = peopleService.update(model, vo);
    }

    @Test
    public void stage3UpdateVariant02() {
        StringBuffer value = new StringBuffer();
        for (int i = 0; i <= 50; i++) {
            value.append("A");
        }

        PeopleRequestVO update = peopleRequestVO;
        People model = peopleRepository.findByName(update.getName());

        PeopleRequestVO vo = new PeopleRequestVO();
        vo.setAddress(value.toString());
        vo.setVersion(model.getVersion());

        exception.expect(BackendExampleException.class);
        exception.expectMessage("Address maximum 50 character");
        PeopleResponseVO result = peopleService.update(model, vo);
    }

    @Test
    public void stage4Delete() {

        People deleted = peopleRepository.findByName("name");

        assertTrue(peopleService.delete(deleted.getSecureId()));
    }
}
